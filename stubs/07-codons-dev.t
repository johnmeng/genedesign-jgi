#! /usr/bin/perl -T

use Test::More tests => 6;

use Bio::GeneDesign;
use Bio::GeneDesign::Codons qw(:GD);

use strict;
use warnings;

my $GD = Bio::GeneDesign->new();
$GD->set_organism(-organism_name => "yeast",
                  -table_path => "codon_tables/Standard.ct",
                  -rscu_path => "codon_tables/yeast.rscu");

my $tCT = $GD->codontable;
my $tRSCU = $GD->rscutable;
my $orf = "ATGGACAGATCTTGGAAGCAGAAGCTGAACCGCGACACCGTGAAGCTGACCGAGGTGATGACCTGGA";
$orf .= "GAAGACCCGCCGCTAAATGGTTTTATACTTTAATTAATGCTAATTATTTGCCACCATGCCCACCCGACC";
$orf .= "ACCAAGATCACCGGCAGCAACAACTACCTGAGCCTGATCAGCCTGAACATCAACGGCCTGAACAGCCCC";
$orf .= "ATCAAGCGGCACCGCCTGACCGACTGGCTGCACAAGCAGGACCCCACCTTCTGTTGCCTCCAGGAGACC";
$orf .= "CACCTGCGCGAGAAGGACCGGCACTACCTGCGGGTGAAGGGCTGGAAGACCATCTTTCAGGCCAACGGC";
$orf .= "CTGAAGAAGCAGGCTGGCGTGGCCATCCTGATCAGCGACAAGATCGACTTCCAGCCCAAGGTGATCAAG";
$orf .= "AAGGACAAGGAGGGCCACTTCATCCTGATCAAGGGCAAGATCCTGCAGGAGGAGCTGAGCATTCTGAAC";
$orf .= "ATCTACGCCCCCAACGCCCGCGCCGCCACCTTCATCAAGGACACCCTCGTGAAGCTGAAGGCCCACATC";
$orf .= "GCTCCCCACACCATCATCGTCGGCGACCTGAACACCCCCCTGAGCAGTGA";
my $shortamb = "ABGCDT";
my $shortorf = "ATGGACAGATCTTGGAAGCAGAAGCTGAACCGC";
my $notorf = "ATGGGATAGTGAAATATG";
my $nostop = "ATGGGTAACCAATTA";

# TESTING find_in_frame()
subtest "find in frame" => sub
{
  plan tests => 2;
  
  my $tstops = _find_in_frame($orf, "*", $tCT);
  my $rstops = {597 => "TGA"};
  is_deeply ($tstops, $rstops, "find stop in frame");
  
  $tstops = _find_in_frame($orf, "I", $tCT);
  $rstops = {99 => "ATT", };
  is_deeply($tstops, $rstops, "find aa in frame");
};

#TESTING orf_finder()
my $torfmap = orf_finder($orf, $tCT);
my $rorfmap = [[1, 0, 199], [2, 34, 165], [-1, 11, 27], [-1, 39, 50]];
is_deeply($torfmap, $rorfmap, "ORF finding");

#TESTING is_ORF()
subtest "is ORF" => sub
{
  plan tests => 3;

  my $tisorf = is_ORF($orf, $tCT);
  my $tisntorf = is_ORF($notorf, $tCT);
  my $tisorf2 = is_ORF($nostop, $tCT);
  is($tisorf, 1, "true ORF");
  is($tisntorf, 0, "not an ORF");
  is($tisorf, 1, "stopless ORF");
}
