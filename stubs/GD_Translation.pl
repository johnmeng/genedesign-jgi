#!/usr/bin/env perl

**USELIB**
use Bio::GeneDesign::Basic qw(configure_GeneDesign complement $ambnt);
use Bio::GeneDesign::Codons qw(:all);
use Bio::SeqIO;
use Bio::Seq;
use Config::Auto;
use Getopt::Long;
use Pod::Usage;
use File::Basename;

use strict;
use warnings;

my $VERSION = '4.00';
my $GDV = "GD_Translation_$VERSION";

local $| = 1;

##Get Arguments
my %p = ();
GetOptions (
      'input=s'     => \$p{INPUT},
      'organism=s'  => \$p{ORG},
      'frame=i'     => \$p{FRAME},
      'reverse'     => \$p{RC},
      'threeframes' => \$p{TF},
      'sixframes'   => \$p{SF},
      'help'        => \$p{HELP}
);
pod2usage(-verbose=>99, -sections=>"DESCRIPTION|ARGUMENTS") if ($p{HELP});

################################################################################
################################ SANITY  CHECK #################################
################################################################################
my $GD = configure_GeneDesign("**CONFLOC**");

die "\n ERROR: You must supply an input FASTA file.\n"
  if (! $p{INPUT});

die "\n ERROR: $p{INPUT} does not exist.\n"
  if (! -e $p{INPUT});

die "\n ERROR: Don't provide -f and -t and -s in the same command, please.\n"
  if (($p{FRAME} && $p{SF})||($p{FRAME} && $p{TF})||($p{SF} && $p{TF}));

warn "\n WARNING: You don't need -r and -s in the same command.\n"
  if ($p{SF} && $p{RC});

if ($p{ORG})
{
  warn "\n WARNING: $p{ORGS} does not have a special codon table; the standard"
       . " codon table will be used.\n"
    unless exists($GD->{CODONTABLES}->{$p{ORG}});
}

if ($p{FRAME} && $p{FRAME} != 1 && $p{FRAME} != 2 && $p{FRAME} != 3)
{
  warn "$p{FRAME} is not a frame argument I understand. I will translate the "
      . "first frame only.\n";
  $p{FRAME} = 1;
}

################################################################################
################################# CONFIGURING ##################################
################################################################################
$p{FRAME} = $p{FRAME} ? $p{FRAME}  : 1;
$p{FRAME} = 0 - $p{FRAME} if ($p{RC});
my @FRAMES = ($p{FRAME});
if ($p{SF})
{
  @FRAMES = qw (1 2 3 -1 -2 -3);
}
elsif ($p{TF})
{
  @FRAMES = qw (1 2 3);
  push @FRAMES, qw(-1 -2 -3) if ($p{RC});
}

my $org = $p{ORG} || "Standard";
my $CODON_TABLE = define_codon_table($org, $GD);

my $FIN = Bio::SeqIO->new(-file => $p{INPUT}, -format => 'FASTA');

my $inputfilename = basename($p{INPUT});
$inputfilename =~ s{\.[^.]+$}{};

################################################################################
################################# TRANSLATION ##################################
################################################################################
my $outputfilename = $inputfilename . "_GDt.FASTA";
my $outputpath = $GD->{output_dir} . "/" . $outputfilename;
open (my $OUTFH, ">" . $outputpath );
my $FOUT = Bio::SeqIO->new(-fh=>$OUTFH, -format=>'FASTA', -width => 80);

while ( my $obj = $FIN->next_seq() )
{
  my $nucseq = uc $obj->seq;
  if ($nucseq =~ $ambnt)
  {
    foreach my $frame (@FRAMES)
    {
      my @peparr = sort(amb_translation($nucseq, $frame, $CODON_TABLE));
      foreach my $pep (@peparr)
      {
        my $desc = $obj->desc . " ambiguous translation, frame $frame ($GDV)";
        my $newobj = Bio::Seq->new(-seq => $pep, -id => $obj->id, -alphabet => 'protein', -desc => $desc);
        $FOUT->write_seq($newobj);
      }
    }
  }
  else
  {
    foreach my $frame (@FRAMES)
    {
      my $pep = translate($nucseq, $frame, $CODON_TABLE);
      my $desc = $obj->desc . " translation, frame $frame ($GDV)";
      my $newobj = Bio::Seq->new(-seq => $pep, -id => $obj->id, -alphabet => 'protein', -desc => $desc);
      $FOUT->write_seq($newobj);
    }
  }

}

close $OUTFH;

print "\n";
print "Output written to $outputpath\n";
print "\n";

exit;

__END__

=head1 NAME

  GD_Translation.pl

=head1 VERSION

  Version 4.00

=head1 DESCRIPTION

  Given at least one nucleotide sequence that may contain ambiguous bases, the
  Translation script generates every possible peptide sequence that can be
  encoded by the nucleotide input. This output is restricted to frame 1 by
  default.

  Output will be named according to the name of the FASTA input file, and will
  be tagged with the GDat suffix.

=head1 USAGE

  Translate every sequence in foo.FASTA in the second frame
    GD_Translation.pl -i foo.FASTA -f 2

  Translate in the -3 frame
    GD_Translation.pl -i foo.FASTA -f 3 -r

  Translate all six frames
    GD_Translation.pl -i foo.FASTA -s

  Translate three reverse frames
    GD_Translation.pl -i foo.FASTA -t -r

=head1 ARGUMENTS

Required arguments:

  -i,   --input : a FASTA file containing nucleotide sequences.

Optional arguments:
  -o,   --organism : an organism whose specialized codon table can be found in
      the config directory; if no table is found the standard codon table will
      be used.
  -f,   --frame : [1, 2, 3] Which frame should be translated. If this argument
      or -a is  not given, only the first frame will be translated.
  -t,   --threeframes : Translate three frames (1, 2, and 3)
  -r,   --reversecomplement : Translate the reverse complement of each input
      sequence. A translation of frame 1 with the -r flag becomes a translation
      of frame -1. A translation with the -a flag and the -r flag becomes a
      translation of frames -1, -2, and -3
  -s,   --sixframes : Translate all six frames (1, 2, 3, -1, -2, and -3)
  -h,   --help : Display this message

=cut