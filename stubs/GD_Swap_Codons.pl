#!/usr/bin/env perl

use Bio::GeneDesign;
use Getopt::Long;
use Pod::Usage;
use File::Basename;

use strict;
use warnings;

my $VERSION = '5.50';
my $GDV = "GD_Swap_Codons_$VERSION";
my $GDS = "_CJ";

local $| = 1;

##Get Arguments
my %p = ();
GetOptions (
      'input=s'     => \$p{INPUT},
      'output=s'    => \$p{OUTPUT},
      'format=s'    => \$p{FORMAT},
      'organism=s'  => \$p{ORG},
      'split'       => \$p{SPLIT},
      'from'        => \$p{FROM},
      'to'          => \$p{TO},
      'help'        => \$p{HELP}
);

################################################################################
################################ SANITY  CHECK #################################
################################################################################
pod2usage(-verbose=>99, -sections=>"NAME|VERSION|DESCRIPTION|ARGUMENTS|USAGE")
  if ($p{HELP});

my $GD = Bio::GeneDesign->new();

#The input file must exist and be a format we care to read.
die "\n GDERROR: You must supply an input file.\n"
  if (! $p{INPUT});
my $iterator = $GD->import_seqs($p{INPUT});
my ($filename, $dirs, $suffix) = fileparse($p{INPUT}, qr/\.[^.]*/);

$p{FORMAT} = $p{FORMAT} || $suffix || "genbank";

#The output path must exist, and we'll need it to end with a slash
$p{OUTPUT} = $p{OUTPUT} || ".";
$p{OUTPUT} .= "/" if (substr($p{OUTPUT}, -1, 1) !~ /[\/]/);
die "\n GDERROR: $p{OUTPUT} does not exist.\n"
  if ($p{OUTPUT} && ! -e $p{OUTPUT});

################################################################################
################################# CONFIGURING ##################################
################################################################################
my @fileswritten;
my @seqstowrite;

################################################################################
############################### CODON  JUGGLING ################################
################################################################################
while ( my $obj = $iterator->next_seq() )
{
  $GD->set_organism(
      -organism_name => $p{ORG}
  );
  my $newobj = $GD->codon_swap(
      -sequence  => $obj,
      -old_codon => $p{FROM},
      -new_codon => $p{TO}
  );
  if ($p{SPLIT})
  {
    my $outputfilename = $newobj->id . $GDS . "." . $p{FORMAT};
    $GD->export_seqs(-filename  => $outputfilename,
                     -sequences => [$newobj],
                     -format    => $p{FORMAT});
    push @fileswritten, $outputfilename;
  }
  else
  {
    push @seqstowrite, $newobj;
  }
}
if (scalar @seqstowrite)
{
  my $outputfilename = $filename . $GDS . "." . $p{FORMAT};
  $GD->export_seqs( -filename  => $outputfilename,
                    -sequences => \@seqstowrite,
                    -format    => $p{FORMAT});
  push @fileswritten, $outputfilename;
}

print "\n";
print "Wrote " . $p{OUTPUT} . "$_\n" foreach @fileswritten;
print "\n";
print $GD->attitude() . " brought to you by $GDV\n\n";

exit;

__END__

=head1 NAME

  GD_Swap_Codons.pl

=head1 VERSION

  Version 5.50

=head1 DESCRIPTION

  Given at least one protein-coding gene as input, the Juggle_Codons script can
  use several algorithms to modify the sequence without altering its
  translation. It is thus possible to generate a sequence that is optimized for
  expression, as different as possible from the original sequence, or some
  combination of the two.

=head1 USAGE

=head1 ARGUMENTS

Required arguments:

  -i,   --input : a file containing nucleotide sequences.
  -org, --organism : an organism whose codon table can be found in the config

Optional arguments:

  -out, --output : path to an output directory
  -f,   --format : default genbank
  -s,   --split : output all sequences as separate files
  -h,   --help : Display this message

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2013, GeneDesign developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
