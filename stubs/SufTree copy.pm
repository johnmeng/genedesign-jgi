#
# GeneDesign module for sequence segmentation
#

=head1 NAME

GeneDesign::SufTree - A suffix tree implementation for nucleotide searching

=head1 VERSION

Version 5.50

=head1 DESCRIPTION

  GeneDesign uses this object to parse peptide sequences for restriction enzyme
  recognition site possibilities

=head1 AUTHOR

Sarah Richardson <SMRichardson@lbl.gov>

=cut

package Bio::GeneDesign::SufTree;

use 5.006;
use strict;
use warnings;

my $VERSION = 5.50;

=head1 Functions

=head2 new

=cut

sub new
{
	my ($class) = @_;
	my $self = { root => {} };
	bless $self, $class;
	return $self;
}

=head2 add_suffix

=cut

sub add_suffix
{
  my ($self, $peptide, $id) = @_;
  my @ids = ref($id) eq "ARRAY" ? @$id  : ($id);
  my $next = $self->{ root };
  my $offset = 0;
  my $len = length($peptide);
  while ($offset < $len)
  {
    my $aa = substr($peptide, $offset, 1);
    $next->{$aa} = {} unless exists $next->{$aa};
    $next = $next->{$aa};
    $offset++;
  }
  $next->{ids} = {} unless exists $next->{ids};
  $next->{ids}->{$_}++ foreach @ids;
  $next->{peptide} = $peptide;
  return;
}

=head2 find_suffixes()

=cut

sub find_suffixes
{
  my ($self, $proteinseq) = @_;
  my @locations;
  my @seq = split('', $proteinseq);
  my $limit = scalar(@seq);
  for my $seq_idx (0..$limit)
  {
    my $cur_idx = $seq_idx;
    my $ref_idx = $seq[$seq_idx];
    my $ref     = $self->{root};
    while (++$cur_idx < $limit and $ref)
    {
      if ($ref->{ids})
      {
        foreach my $id (keys %{$ref->{ids}})
        {
          push @locations, [$id, $seq_idx + 1, $ref->{peptide}];
        }
      }
      $ref_idx = $seq[$cur_idx];
      $ref = $ref->{$ref_idx};
    }
  }
  return @locations;
}

=head2 search_suffix_trees()

Searches suffix trees

  Arguments: an array reference full of L<Bio::GeneDesign::SufTree> objects
             a nucleotide sequence,
             a peptide sequence translated from the nucleotide sequence,
             a GeneDesign codon table hashref
             optionally, a hashref of previously calculated ambiguous
                translation results

  Returns: array reference full of hits

  #NO UNIT TESTS

=cut

sub search_suffix_trees
{
  my ($treelist, $nucseq, $aaseq, $CODON_TABLE, $xlationref) = @_;
  my $results;
  my $flip = 0;
  foreach my $tree (@$treelist)
  {
  #print "\tSearching $flip tree...\n" if ($debug);
    $flip++;
    foreach my $rog ($tree->find_suffixes($aaseq))
    {
      my $enzyme    = $$rog[0];
      my $enz       = $enzyme->id;
      my $recsite   = $enzyme->recseq();
         $recsite   = _complement($recsite, 1) if ($flip > 1);
      my $nucstart  = $$rog[1] * 3;
      my $peptide   = $$rog[2];
      my $presence  = "p";
      my $ohang     = {};
      my ($mattersbit, $ohangstart, $ohangend, $fabric) = (q{}, 0, 0, q{});
      my ($offset, $situ, $pproof, $sitelen) = (0, q{}, q{}, 0);

      #print "\tgot $enz @ $nucstart, $peptide\n" if ($debug);
    ##Figure Possible overhangs
      if ($enzyme->type() eq "b" || $enzyme->class() eq "IIB")
      {
        $situ = substr($nucseq, $nucstart, length($peptide)*3);
        $pproof = translate($situ, 1, $CODON_TABLE);
        $ohangstart = 0;
      }
      elsif ($enzyme->class() eq "IIP")
      {
        my ($lef, $rig) = (length($1), length($2)) if ($enzyme->cutseq() =~ $Bio::GeneDesign::RestrictionEnzyme::IIPreg);
        ($lef, $rig) = ($rig, $lef) if ($rig < $lef);
        $ohangstart = $enzyme->len - $rig + 1;
        $ohangend = $enzyme->len - $lef;
        $situ = substr($nucseq, $nucstart, length($peptide)*3);
        ($fabric, $offset) = pattern_aligner($situ, $recsite, $peptide, $CODON_TABLE, 1, $xlationref);
        $mattersbit = substr($situ, $ohangstart + $offset -1, $ohangend - $ohangstart + 1);
        $pproof = translate($situ, 1, $CODON_TABLE);
        $sitelen = $enzyme->len;
      }
      elsif ($enzyme->class() eq "IIA")
      {
##NOTE: when the overhang is outside the gene being parsed, this is going to
## throw a substr outside of string error, lines 278 and 298
        my ($lef, $rig) = ($1, $2)  if ($enzyme->cutseq() =~ $Bio::GeneDesign::RestrictionEnzyme::IIAreg);
        ($rig, $lef) = ($lef, $rig) if ($rig < $lef);
        $sitelen = $rig >= 0 ? $enzyme->len + $rig  : $enzyme->len;
        my $nuclen = length($peptide)*3;
        $nuclen++ while($nuclen % 3 != 0);
        $situ = substr($nucseq, $nucstart, $nuclen);
        ($fabric, $offset) = pattern_aligner($situ, $recsite, $peptide, $CODON_TABLE, 1, $xlationref);
        my $add;
        if ($flip == 1)
        {
          $ohangstart = $enzyme->len + $lef + 1;
          if ($rig > 0)
          {
            $add = $rig - (length($fabric) - ($offset + $enzyme->len));
            $add ++ while ($add % 3 != 0);
            $situ .= substr($nucseq, $nucstart + $nuclen, $add);
            $fabric .= "N" x $add;
          }

        }
        else
        {
          if ($rig > 0)
          {
            $add =  $rig - $offset;
            $add ++ while ($add % 3 != 0);
            $situ = substr($nucseq, $nucstart - $add, $add) . $situ;
            $fabric = "N" x $add . $fabric;
            $nucstart = $nucstart - $add;
            $ohangstart = $add - $rig + 1;
          }
          else
          {
            $ohangstart = $offset + abs($rig) + 1;
          }
        }
        $mattersbit = substr($nucseq, $nucstart + $ohangstart+1, $rig-$lef);
        $pproof = translate($situ, 1, $CODON_TABLE);
      }
      else
      {
        print "I don't recognize this type of enzyme: " . $enzyme->id;
      }
      unless ($enzyme->type() eq "b" || $enzyme->class() eq "IIB")
      {
        if ($fabric eq "0")
        {
          print "oh no bad fabric, $enz, $fabric, $peptide\n";
          next;
        }
        my $lenm = $mattersbit  ? length($mattersbit) : 0;
        my $matstart = $ohangstart + $offset - 1;
           $matstart-- while($matstart % 3 != 0);
        my $matend = $ohangstart + $offset + $lenm - 1;
           $matend++ while($matend % 3 != 2);
        my $matlen = $matend - $matstart + 1;
        my $peproof = substr($pproof, ($matstart/3), $matlen/3);
        my $what = substr($fabric, $matstart, $matlen);
      #print "\n\n$flip $enz, $peptide, fab: $fabric, situ: $situ, pproof: $pproof, " if ($debug);
      #print "ohangstart: $ohangstart, ohangend: $ohangend, matters: $mattersbit, " if ($debug);
      #print "offset: $offset, matstart: $matstart, matlen: $matlen, peproof: $peproof, what: $what\n" if ($debug);
        foreach my $swapseq (amb_transcription($what, $CODON_TABLE, $peproof))
        {
          substr($fabric, $matstart, $matlen, $swapseq);
          my $tohang = substr($fabric, $ohangstart +  $offset - 1, $lenm);
        #print "\t\t\tsubbing $swapseq into $fabric\tconsidering $tohang\n" if ($debug);
          $$ohang{$tohang}++ if ($tohang);#if ($tohang ne _complement($tohang, 1));
        }
      }

    ##Determine Presence
      $presence = "e" if ($situ =~ $enzyme->regex()->[$flip - 1]);
      #print "got $enz @ $nucstart, $presence, $peptide, $fabric\n" if ($presence eq "e" && $debug);
      unless ($enzyme->class() eq "IIB" && $presence eq "p")
      {
        my $ohangoffset = $ohangstart + $offset - 1;
        push @$results, [$enzyme, $nucstart, $presence, $sitelen, $ohang, $pproof, $ohangoffset, $mattersbit, $flip];
      }
    }
  }
  return $results;
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2013, GeneDesign developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
